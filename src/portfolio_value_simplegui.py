'''
Things seem to get complicated fast when using manual GUI creation.
***
Display Portfolio Value report using PySimpleGUI.
Cache it in temp folder. Allow regeneration.
'''
import PySimpleGUIWeb as sg
from gnucash_portfolio.reports.portfolio_models import PortfolioValueViewModel

def create_layout(model: PortfolioValueViewModel):
    ''' Create the window layout '''
    from gnucash_portfolio.reports.portfolio_models import StockViewModel

    layout = [
        [sg.Text("Portfolio Value Report")]
    ]

    # add rows

    rows = sorted(model.stock_rows, key=lambda row: f"{row.exchange}:{row.symbol}")
    table_rows = [[]]
    for row in rows:
        col1 = f"{row.exchange}:{row.symbol}"
        col2 = f"{row.shares_num:,} @ {row.avg_price:,.2f} {row.currency} = {row.cost:>7,.2f}"
        col3 = f"@ {row.price:,.2f} = {row.balance:>9,.2f}"
        col4 = f"{row.gain_loss:,.2f}"
        col5 = f"{row.gain_loss_perc:,.2f}%"
        col6 = f"{row.income:,.2f}"
        col7 = f"{row.income_last_12m:,.2f}"
        col8 = f"{row.income_last_12m_perc:,.2f}"

        table_row = [col1, col2, col3, col4, col5, col6, col7, col8]
        
        table_rows.append(table_row)

        #layout += [[sg.Text(col1)]]

    layout += [[sg.Table(table_rows,
        #col_widths=[20, 200, 20, 20, 20, 20, 20, 20],
        #size="100%",
        #max_col_width=150,
        auto_size_columns=True,
        display_row_numbers=True,
        num_rows=20)]]

    # close button
    layout += [[sg.Button("Exit")]]

    return layout

def show_gui(model: PortfolioValueViewModel):
    ''' Start the GUI '''
    layout = create_layout(model)
    port = 8080

    window = sg.Window("Portfolio Value", 
        web_start_browser=False,
        web_port=port
        ).Layout(layout)

    print(f"starting on port {port}")
    while True:
        e, v = window.Read()

        if e is None or e == 'Exit':
            break

    window.Close()

def load_data() -> PortfolioValueViewModel:
    ''' fetch the data '''
    from pydatum import Datum
    from gnucash_portfolio.reports import portfolio_value
    from gnucash_portfolio.reports.portfolio_models import PortfolioValueInputModel

    input_model = PortfolioValueInputModel()
    input_model.as_of_date = Datum().today()

    result = portfolio_value.run(input_model)
    return result

def main():
    ''' The main '''
    # load data
    model = load_data()

    show_gui(model)

if (__name__ == "__main__"):
    main()
