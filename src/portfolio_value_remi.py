
# -*- coding: utf-8 -*-

import remi.gui as gui
from remi.gui import *
from remi import start, App


class PortfolioValueReport(App):
    def __init__(self, *args, **kwargs):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        if not 'editing_mode' in kwargs.keys():
            super(PortfolioValueReport, self).__init__(*args, static_file_path={'my_res':'./res/'})

    def idle(self):
        #idle function called every update cycle
        pass
    
    def main(self):
        return PortfolioValueReport.construct_ui(self)
        
    @staticmethod
    def construct_ui(self):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        container = Widget()
        container.attributes.update({"class":"Widget","editor_constructor":"()",
            "editor_varname":"container","editor_tag_type":"widget","editor_newclass":"False",
            "editor_baseclass":"Widget"})
        container.style.update({"margin":"0px","width":"92%","height":"281px",
            "top":"36.px","left":"48px","position":"absolute",
            "overflow":"auto"})
        titleLabel = Label('Portfolio Value Report')
        titleLabel.attributes.update({"class":"Label",
            "editor_constructor":"('Portfolio Value Report')","editor_varname":"titleLabel",
            "editor_tag_type":"widget","editor_newclass":"False","editor_baseclass":"Label"})
        titleLabel.style.update({"margin":"0px","height":"26px","top":"20px","left":"30px",
            "position":"absolute","overflow":"auto"})
        container.append(titleLabel,'titleLabel')

        # Table
        table = gui.TableWidget(10, 3, True, editable=True, width="100%", height=300)
        table.style['font-size'] = '8px'

        # self.fill_table(table, table)

        container.append(table)

        self.container = container
        return self.container
    


#Configuration
configuration = {'config_project_name': 'PortfolioValueReport', 'config_address': '0.0.0.0', 
    'config_port': 8080, 'config_multiple_instance': True, 'config_enable_file_cache': True, 
    'config_start_browser': False, 'config_resourcepath': './res/'}

if __name__ == "__main__":
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    start(PortfolioValueReport, address=configuration['config_address'], 
        port=configuration['config_port'], 
        multiple_instance=configuration['config_multiple_instance'], 
        enable_file_cache=configuration['config_enable_file_cache'],
        start_browser=configuration['config_start_browser'])
