# gnucash-portfolio-simplegui

The UI for GnuCash Portfolio, implemented with PySimpleGUI. More specifically, the intention is to use the PySimpleGUIWeb version as that is the only one that reliably works on Android.
